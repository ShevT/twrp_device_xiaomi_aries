TARGET_OTA_ASSERT_DEVICE := aries

# Target arch settings
TARGET_ARCH := arm
TARGET_NO_BOOTLOADER := true
TARGET_BOARD_PLATFORM := msm8960
TARGET_CPU_ABI := armeabi-v7a
TARGET_CPU_ABI2 := armeabi
TARGET_ARCH_VARIANT := armv7-a-neon
TARGET_CPU_VARIANT := krait
TARGET_CPU_SMP := true
TARGET_BOOTLOADER_BOARD_NAME := aries

# Kernel
TARGET_PREBUILT_KERNEL := device/xiaomi/aries/kernel
BOARD_KERNEL_CMDLINE := console=null androidboot.hardware=qcom ehci-hcd.park=3 maxcpus=2 androidboot.bootdevice=msm_sdcc.1 androidboot.selinux=permissive
BOARD_KERNEL_BASE := 0x80200000
BOARD_KERNEL_PAGESIZE := 2048
BOARD_MKBOOTIMG_ARGS := --ramdisk_offset 0x02000000

# Fix this up by examining /proc/partitions on a running device. (value * 1024)
BOARD_BOOTIMAGE_PARTITION_SIZE := 15728640
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 15728640
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 536870912
# New partition
BOARD_USERDATAIMAGE_PARTITION_SIZE := 29456579584
BOARD_FLASH_BLOCK_SIZE := 131072

# fstab
TARGET_RECOVERY_FSTAB := device/xiaomi/aries/fstab.qcom
RECOVERY_FSTAB_VERSION := 2

# File Systems
TARGET_USERIMAGES_USE_F2FS := true
TARGET_USERIMAGES_USE_EXT4 := true
BOARD_HAS_LARGE_FILESYSTEM := true
BOARD_SUPPRESS_SECURE_ERASE := true

# Graphics
BRIGHTNESS_SYS_FILE := /sys/class/leds/lcd-backlight/brightness
RECOVERY_GRAPHICS_FORCE_USE_LINELENGTH := true
TW_SCREEN_BLANK_ON_BOOT := true
TW_THEME := portrait_hdpi

# Storages
TW_INTERNAL_STORAGE_PATH := "/sdcard"
TW_INTERNAL_STORAGE_MOUNT_POINT := "sdcard"
TW_FLASH_FROM_STORAGE := true
BOARD_HAS_NO_REAL_SDCARD := true
TW_NO_USB_STORAGE := true

# Other
BOARD_HAS_NO_SELECT_BUTTON := true
# Enable Download mode
TW_HAS_DOWNLOAD_MODE := true

# Custom make BOOTIMG
BOARD_CUSTOM_BOOTIMG_MK := device/xiaomi/aries/custombootimg.mk

# Crypto
TW_CRYPTO_REAL_BLKDEV := "/dev/block/platform/msm_sdcc.1/by-name/userdata"
TW_CRYPTO_MNT_POINT := "/data"
TW_CRYPTO_KEY_LOC := "footer"
TW_INCLUDE_L_CRYPTO := true
