LOCAL_PATH := device/xiaomi/aries

ifeq ($(TARGET_PREBUILT_KERNEL),)
	LOCAL_KERNEL := $(LOCAL_PATH)/kernel
else
	LOCAL_KERNEL := $(TARGET_PREBUILT_KERNEL)
endif

PRODUCT_COPY_FILES += \
    $(LOCAL_KERNEL):kernel

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/res/rebootdownload.sh:recovery/root/sbin/rebootdownload.sh

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/fstab.recovery:recovery/root/etc/twrp.fstab

# Necessary to mount a PC
$(call inherit-product, build/target/product/full.mk)

# Release name
PRODUCT_RELEASE_NAME := aries

## Device identifier. This must come after all inclusions
PRODUCT_DEVICE := aries
PRODUCT_NAME := omni_aries
PRODUCT_BRAND := xiaomi
PRODUCT_MODEL := aries
PRODUCT_MANUFACTURER := xiaomi
